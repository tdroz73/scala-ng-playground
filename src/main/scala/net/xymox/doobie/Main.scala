package net.xymox.doobie

import net.xymox.doobie.api.Server
import net.xymox.doobie.environment.Environments.appEnvironment
import zio._

object Main extends App {

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    val program = for {
      _ <- Server.runServer
    } yield ()

    program.provideLayer(appEnvironment).exitCode
  }
}
