package net.xymox.doobie.repositories

import net.xymox.doobie.model.Bill
import zio.{Task, URLayer, ZLayer}

object BillRepository {

  trait Service {
    def listAll: fs2.Stream[Task, Bill]
    def getById(id: Long): Task[Option[Bill]]
  }

  val live: URLayer[DbTransactor, BillRepository] = ZLayer.fromService { resource => BillRepositoryImpl(resource.xa) }
}
