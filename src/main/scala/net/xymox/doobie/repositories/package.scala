package net.xymox.doobie

import doobie.util.transactor.Transactor
import net.xymox.doobie.Configuration.DbConfig
import net.xymox.doobie.model.Bill
import zio.{Has, RIO, Task, URLayer, ZLayer}
import zio.interop.catz._

package object repositories {
  type DbTransactor = Has[DbTransactor.Resource]
  type BillRepository = Has[BillRepository.Service]

  // TODO - refactor this so that each repository has its own sub-package and thus keeps this 'clean'; can get out of hand easily
  def allBills: RIO[BillRepository, fs2.Stream[Task, Bill]] = RIO.access(_.get.listAll)
  def getById(id: Long): RIO[BillRepository, Task[Option[Bill]]] = RIO.access(_.get.getById(id))

  object DbTransactor {

    trait Resource {
      val xa: Transactor[Task]
    }

    val postgres: URLayer[Has[DbConfig], DbTransactor] = ZLayer.fromService { cfg =>
      new Resource {
        override val xa: Transactor[Task] = Transactor.fromDriverManager(driver = cfg.driver, url = cfg.jdbcUrl, user = cfg.user, pass = cfg.password)
      }
    }
  }
}
