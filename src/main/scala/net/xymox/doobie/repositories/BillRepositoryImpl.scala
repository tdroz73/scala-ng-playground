package net.xymox.doobie.repositories

import doobie.implicits._
import doobie.quill.DoobieContext
import doobie.util.transactor.Transactor
import io.getquill._
import net.xymox.doobie.model.Bill
import zio.Task
import zio.interop.catz._

private[repositories] final case class BillRepositoryImpl(xa: Transactor[Task]) extends BillRepository.Service {
  val ctx = new DoobieContext.Postgres(SnakeCase)

  import ctx._

  override def listAll: fs2.Stream[Task, Bill] = ctx.stream(Queries.allBills).transact(xa)

  override def getById(id: Long): Task[Option[Bill]] = ctx.run(Queries.findById(id)).transact(xa).map(_.headOption)

  object Queries {
    val allBills: ctx.Quoted[ctx.EntityQuery[Bill]] = quote(querySchema[Bill]("bills"))
    def findById(id: Long): ctx.Quoted[ctx.EntityQuery[Bill]] = quote { allBills.filter(_.id == lift(Option(id))) }

  }
}
