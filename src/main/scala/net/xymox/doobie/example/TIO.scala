package net.xymox.doobie.example

import net.xymox.doobie.example.TIO.{Effect, Fail, FlatMap, Recover}

import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

sealed trait TIO[+A] {
  def flatMap[B](f: A => TIO[B]): TIO[B] = TIO.FlatMap(this, f)
  def map[B](f: A => B): TIO[B] = flatMap(a => TIO.succeed(f(a)))
  def *>[B](that: TIO[B]): TIO[B] = flatMap(_ => that)
  def recover[B >: A](f: Throwable => TIO[B]): TIO[B] = TIO.Recover(this, f)
}

object TIO {
  case class Effect[+A](a: () => A) extends TIO[A]
  case class FlatMap[A, B](tio: TIO[A], f: A => TIO[B]) extends TIO[B]
  case class Fail[A](e: Throwable) extends TIO[A]
  case class Recover[A](tio: TIO[A], f: Throwable => TIO[A]) extends TIO[A]

  def succeed[A](a: A): TIO[A] = Effect(() => a)
  def effect[A](a: => A): TIO[A] = Effect(() => a)
  def fail[A](throwable: Throwable): TIO[A] = Fail(throwable)
}

trait Runtime {
  def unsafeRunSync[A](tio: TIO[Any]): Try[Any]
}

object Runtime extends Runtime {
  override def unsafeRunSync[A](tio: TIO[Any]): Try[Any] = eval(tio)

  private def eval[A](tio: TIO[A]): Try[A] = {
    tio match {
      case Effect(a) => Try(a())
      case FlatMap(tio, f: (Any => TIO[Any])) =>
        eval[Any](tio) match {
          case Success(res) => eval(f(res))
          case Failure(e)   => Failure(e)
        }
      case Fail(t) => Failure(t)
      case Recover(tio, f) =>
        eval(tio) match {
          case Failure(e) => eval(f(e))
          case success    => success
        }
    }
  }
}

object Console {
  def putStnLn(str: => String): TIO[Unit] = TIO.effect(println(str))
}

trait TIOApp {
  def run: TIO[Any]
  final def main(args: Array[String]): Unit = Runtime.unsafeRunSync(run).get
}

object SequenceEffects extends TIOApp {

  override def run: TIO[Any] =
    for {
      _ <- TIO.effect(println("My first effect"))
      _ <- TIO.effect(println("My second effect"))
    } yield ()
}

object ExampleWithThrow extends TIOApp {
  import Console._

  override def run: TIO[Any] =
    for {
      _ <- putStnLn("First effect")
      _ <- TIO.effect(throw new RuntimeException("No bueno."))
      _ <- putStnLn("Second effect")
    } yield ()
}

object FailAndRecover extends TIOApp {
  import Console._

  override def run: TIO[Any] = {
    (
      for {
        _ <- putStnLn("First effect")
        _ <- TIO.fail(new RuntimeException("boom"))
        _ <- putStnLn("second effect")
      } yield ()
    ).recover {
      case NonFatal(e) => putStnLn(s"Recovered from: ${e.getClass.getName}")
    }
  }
}
