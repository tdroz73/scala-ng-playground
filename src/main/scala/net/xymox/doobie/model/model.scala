package net.xymox.doobie.model

import java.time.Instant

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

case class Payee(id: Option[Long], name: String, url: String, createdAt: Option[Instant])

object Payee {
  implicit val encoder: Encoder[Payee] = deriveEncoder[Payee]
}
case class Bill(id: Option[Long], payeeId: Long, dayOfMonth: Int, minAmountDue: Double)

object Bill {
  implicit val encoder: Encoder[Bill] = deriveEncoder[Bill]
}
