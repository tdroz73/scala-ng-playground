package net.xymox.doobie

import net.xymox.doobie.Configuration.{DbConfig, HttpServerConfig}
import zio.Has

package object config {
  type Configuration = Has[HttpServerConfig] with Has[DbConfig]
}
