package net.xymox.doobie

import pureconfig._
import pureconfig.generic.auto._
import zio.{Has, ULayer, ZIO, ZLayer}

object Configuration {
  type Configuration = Has[HttpServerConfig] with Has[DbConfig]

  case class DbConfig(driver: String, jdbcUrl: String, user: String, password: String)
  case class HttpServerConfig(host: String, port: Int, path: String)
  case class AppConfig(dbConfig: DbConfig, httpServerConfig: HttpServerConfig)

  val live: ULayer[Configuration] = ZLayer.fromEffectMany(
    ZIO
      .effect(ConfigSource.default.loadOrThrow[AppConfig])
      .map(c => Has(c.httpServerConfig) ++ Has(c.dbConfig))
      .orDie
  )

}
