package net.xymox.doobie.environment

import net.xymox.doobie.Configuration
import net.xymox.doobie.Configuration.Configuration
import net.xymox.doobie.repositories.{BillRepository, BillRepositoryImpl, DbTransactor}
import zio.ULayer
import zio.clock.Clock

object Environments {
  type HttpServerEnvironment = Configuration with Clock
  type AppEnvironment = HttpServerEnvironment with BillRepository

  val httpServerEnvironment: ULayer[HttpServerEnvironment] = Configuration.live ++ Clock.live
  val dbTransactor: ULayer[DbTransactor] = Configuration.live >>> DbTransactor.postgres
  val billRepository: ULayer[BillRepository] = dbTransactor >>> BillRepository.live
  val appEnvironment: ULayer[AppEnvironment] = httpServerEnvironment ++ billRepository
}
