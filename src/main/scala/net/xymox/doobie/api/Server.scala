package net.xymox.doobie.api

import cats.data.Kleisli
import cats.effect.ExitCode
import cats.implicits._
import org.http4s.implicits._
import net.xymox.doobie.Configuration.HttpServerConfig
import net.xymox.doobie.environment.Environments.AppEnvironment
import net.xymox.doobie.api.endpoints.BillsEndpoint
import net.xymox.doobie.api.endpoints.HealthEndpoint
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.{AutoSlash, CORS, CORSConfig, GZip}
import org.http4s.{HttpRoutes, Request, Response}
import zio.{RIO, ZIO}
import zio.interop.catz._

import scala.concurrent.duration._

object Server {
  type ServerRIO[A] = RIO[AppEnvironment, A]
  type ServerRoutes = Kleisli[ServerRIO, Request[ServerRIO], Response[ServerRIO]]

  def runServer: ZIO[AppEnvironment, Nothing, Unit] = {
    ZIO
      .runtime[AppEnvironment]
      .flatMap { implicit rts =>
        val cfg = rts.environment.get[HttpServerConfig]
        val ec = rts.platform.executor.asEC

        BlazeServerBuilder[ServerRIO](ec)
          .bindHttp(port = cfg.port, host = cfg.host)
          .withHttpApp(createRoutes(cfg.path))
          .serve
          .compile[ServerRIO, ServerRIO, ExitCode]
          .drain
      }
      .orDie
  }

  def createRoutes(basePath: String): ServerRoutes = {
    val healthRoutes = new HealthEndpoint[AppEnvironment].routes
    val billRoutes = new BillsEndpoint[AppEnvironment].routes
    val routes = healthRoutes <+> billRoutes
    Router[ServerRIO](basePath -> middleware(routes)).orNotFound
  }

  private val originConfig = CORSConfig(
    anyOrigin = false,
    allowedOrigins = Set("*"),
    allowCredentials = false,
    maxAge = 1.day.toSeconds
  )

  private val middleware: HttpRoutes[ServerRIO] => HttpRoutes[ServerRIO] = {
    { http: HttpRoutes[ServerRIO] =>
      AutoSlash(http)
    }.andThen { http: HttpRoutes[ServerRIO] =>
      GZip(http)
    }.andThen { http: HttpRoutes[ServerRIO] =>
      CORS(http, originConfig)
    }
  }

}
