package net.xymox.doobie.api.endpoints

import cats.Applicative
import io.circe.Encoder
import net.xymox.doobie.model.Bill
import net.xymox.doobie.repositories.BillRepository
import org.http4s.{EntityEncoder, HttpRoutes}
import org.http4s.circe._
import io.circe.syntax._
import org.http4s.dsl.Http4sDsl
import zio.RIO
import zio.interop.catz._
import net.xymox.doobie.repositories._
import org.http4s.server.Router

class BillsEndpoint[R <: BillRepository] {
  type BillTask[A] = RIO[R, A]
  type BillStream = fs2.Stream[BillTask, Bill]

  implicit def jsonEncoder[F[_]: Applicative, A](implicit encoder: Encoder[A]): EntityEncoder[F, A] =
    jsonEncoderOf[F, A]
  private val prefixPath = "/bills"

  val dsl: Http4sDsl[BillTask] = Http4sDsl[BillTask]
  import dsl._

  private val httpRoutes = HttpRoutes.of[BillTask] {
    case GET -> Root =>
      val pipeline: BillTask[BillStream] = allBills
      for {
        stream <- pipeline
        json <- Ok(stream.map(_.asJson))
      } yield json
    case GET -> Root / IntVar(id) =>
      for {
        bill <- getById(id)
        json <- Ok(bill.map(_.asJson))
      } yield json
  }

  val routes: HttpRoutes[BillTask] = Router(prefixPath -> httpRoutes)
}
