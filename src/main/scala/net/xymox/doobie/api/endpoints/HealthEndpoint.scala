package net.xymox.doobie.api.endpoints

import cats.Applicative
import io.circe.Encoder
import org.http4s.{EntityEncoder, HttpRoutes}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import zio.RIO
import zio.interop.catz._

final class HealthEndpoint[R] {
  type HealthTask[A] = RIO[R, A]
  implicit def jsonEncoder[F[_]: Applicative, A](implicit encoder: Encoder[A]): EntityEncoder[F, A] = jsonEncoderOf[F, A]

  private val prefixPath = "/_health"

  val dsl: Http4sDsl[HealthTask] = Http4sDsl[HealthTask]
  import dsl._

  private val httpRoutes = HttpRoutes.of[HealthTask] {
    case GET -> Root => Ok()
  }

  val routes: HttpRoutes[HealthTask] = Router(prefixPath -> httpRoutes)
}
