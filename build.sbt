name := "scala-ng-playground"

version := "0.1"

scalaVersion := "2.13.3"

val Http4sVersion = "0.21.4"
val CirceVersion = "0.13.0"
val Specs2Version = "4.9.3"
val LogbackVersion = "1.2.3"
val DoobieVersion = "0.9.0"

libraryDependencies ++= Seq(
  "org.http4s"            %% "http4s-blaze-server" % Http4sVersion,
  "org.http4s"            %% "http4s-blaze-client" % Http4sVersion,
  "org.http4s"            %% "http4s-circe"        % Http4sVersion,
  "org.http4s"            %% "http4s-dsl"          % Http4sVersion,
  "io.circe"              %% "circe-core"          % CirceVersion,
  "io.circe"              %% "circe-generic"       % CirceVersion,
  "io.circe"              %% "circe-parser"        % CirceVersion,
  "org.specs2"            %% "specs2-core"         % Specs2Version % "test",
  "ch.qos.logback"         % "logback-classic"     % LogbackVersion,
  "dev.zio"               %% "zio"                 % "1.0.0-RC21-2",
  "dev.zio"               %% "zio-logging"         % "0.3.2",
  "dev.zio"               %% "zio-interop-cats"    % "2.1.3.0-RC16",
  "org.tpolecat"          %% "doobie-core"         % DoobieVersion,
  "org.tpolecat"          %% "doobie-hikari"       % DoobieVersion,
  "org.tpolecat"          %% "doobie-postgres"     % DoobieVersion,
  "org.tpolecat"          %% "doobie-quill"        % DoobieVersion,
  "com.github.pureconfig" %% "pureconfig"          % "0.12.3"
)

addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3")
addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1")

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Xfatal-warnings"
)
